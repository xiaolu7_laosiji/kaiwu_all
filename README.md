# 编译步骤

待填坑

# 运行效果截图

## 启动步骤

需要先运行编译步骤~

1.启动eureka

进入下载目录，例如**E:\git\kaiwu\2020\edu-eureka-boot\edu-eureka-boot-master\target**

运行**java -jar edu-eureka-boot.jar**

2.启动config

进入下载目录，例如**E:\git\kaiwu\2020\edu-config-boot\edu-config-boot-master\target**

运行**java -jar edu-config-boot.jar**

3.启动user

进入下载目录，例如**E:\git\kaiwu\2020\edu-user-boot\edu-user-boot-master\edu-user-impl\target**

运行**java -jar edu-user-boot.jar**

4.启动oauth

进入下载目录，例如**E:\git\kaiwu\2020\edu-oauth-boot\edu-oauth-boot-master\target**

运行**java -jar edu-oauth-boot.jar**

5.进入idea，导入edu-front-boot项目，并启动**com.lagou.edu.front.LagouEduFrontApplication**

## **jps**

使用命令jps验证进程

```
11632 LagouEduFrontApplication
13776 edu-oauth-boot.jar
15252 Jps
7700 edu-eureka-boot.jar
8100
16024 RemoteMavenServer36
13948 edu-config-boot.jar
16284 edu-user-boot.jar
```

## eureka截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/1216/155724_31c16fac_116678.png "eureka.png")

## postman验证

首先，你需要使用

E:\git\kaiwu\2020\edu-front-boot\edu-front-boot-master\src\main\java\com\lagou\edu\front\PasswordTest.java

修改一下数据库中某个用户的密码

之后可以使用idea中的postman先验证一下登陆接口

```
POST http://127.0.0.1:8081/user/login?phone=15510792994&password=6342180
Accept: */*
Cache-Control: no-cache
Content-Type: application/json
```

**查看请求结果**：

```json
{"state":1,"message":"success","content":"{\"access_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMTAwMDMwMDEyIiwidXNlcl9uYW1lIjoiMTU1MTA3OTI5OTQiLCJzY29wZSI6WyJyZWFkIl0sIm9yZ2FuaXphdGlvbiI6IjE1NTEwNzkyOTk0IiwiZXhwIjoxNjA4MTA5NzYxLCJhdXRob3JpdGllcyI6WyJOT05FIl0sImp0aSI6Ii1VTnNFZUZyZFdGODV1VDduTzVwTlRRQl9fQSIsImNsaWVudF9pZCI6InRlc3RfY2xpZW50In0.rXsFlFMTjiNM_Ng36-r1XYqbbQmHw_VYmbQyjsVOcrA\",\"token_type\":\"bearer\",\"refresh_token\":\"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMTAwMDMwMDEyIiwidXNlcl9uYW1lIjoiMTU1MTA3OTI5OTQiLCJzY29wZSI6WyJyZWFkIl0sIm9yZ2FuaXphdGlvbiI6IjE1NTEwNzkyOTk0IiwiYXRpIjoiLVVOc0VlRnJkV0Y4NXVUN25PNXBOVFFCX19BIiwiZXhwIjoxNjA4MjEwNTYxLCJhdXRob3JpdGllcyI6WyJOT05FIl0sImp0aSI6IkZUdm9RaHJYLThOc2xNb3pTYzlLNGQwQjc4OCIsImNsaWVudF9pZCI6InRlc3RfY2xpZW50In0.BDK_Q26dnKLrgxbaxaKte8-Mzv_VRMR90_bJrEC6iSM\",\"expires_in\":5360,\"scope\":\"read\",\"user_id\":\"100030012\",\"organization\":\"15510792994\",\"jti\":\"-UNsEeFrdWF85uT7nO5pNTQB__A\"}","success":true}
```

## 前端运行截图

待填坑

# nacos版本改造

敬请期待