package com.lagou.edu.message.mq.listener;

import java.util.List;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.alibaba.fastjson.JSON;
import com.lagou.edu.common.constant.MQConstant;
import com.lagou.edu.common.mq.dto.BaseMqDTO;
import com.lagou.edu.common.mq.listener.AbstractMqListener;
import com.lagou.edu.common.util.ValidateUtils;
import com.lagou.edu.message.api.dto.Message;
import com.lagou.edu.message.service.IMessageService;

import lombok.extern.slf4j.Slf4j;

/**
 * @author: ma wei long
 * @date:   2020年6月30日 下午12:55:47
*/
@Slf4j
@Component
@RocketMQMessageListener(topic = MQConstant.Topic.LESSON_RELEASE_SEND_MESSAGE, consumerGroup = "${rocketmq.producer.group}" + "_" + MQConstant.Topic.LESSON_RELEASE_SEND_MESSAGE)
public class LessonReleaseSendMessageListener extends AbstractMqListener<BaseMqDTO<List<Integer>>> implements RocketMQListener<BaseMqDTO<List<Integer>>>{
	
	@Autowired
	private IMessageService messageService;
	
	/**
	 * @author: ma wei long
	 * @date:   2020年6月30日 上午10:21:50
	*/
    @Override
    public void onMessage(BaseMqDTO<List<Integer>> data) {
    	log.info("onMessage - data:{}",JSON.toJSONString(data));
    	ValidateUtils.notNullParam(data);
    	ValidateUtils.notNullParam(data.getMessageId());
    	
    	if(this.checkMessageId(data.getMessageId())) {
    		return;
    	}
    	
    	List<Integer> userIdList = data.getData();
    	if(CollectionUtils.isEmpty(userIdList)) {
    		return;
    	}
    	
    	Message message = null;
    	for(Integer userId : userIdList) {
    		message = new Message();
    		message.setUserId(userId);
    		messageService.sendMessage(message);
    	}
    	this.updateMessageId(data.getMessageId());
    }
}